﻿using DangoteSyncService.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Repositories
{
    class RecordRepository : DapperRepository<Records>
    {
        public RecordRepository(IDbConnection connection, ISqlGenerator<Records> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
