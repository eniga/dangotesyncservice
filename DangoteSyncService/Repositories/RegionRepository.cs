﻿using DangoteSyncService.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Repositories
{
    class RegionRepository : DapperRepository<Regions>
    {
        public RegionRepository(IDbConnection connection, ISqlGenerator<Regions> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}
