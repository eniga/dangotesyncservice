﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Models
{
    [Table("tbl_areas")]
    class Areas
    {
        [Key, Identity]
        public int Id { get; set; }
        public string company_code { get; set; }
        public string cbn_code { get; set; }
        public string credit_area { get; set; }
    }
}
