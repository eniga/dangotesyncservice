﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Models
{
    [Table("tbl_regions")]
    class Regions
    {
        [Key, Identity]
        public int Id { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string region_name { get; set; }
    }
}
