﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Models
{
    [Table("tbl_logs")]
    public class ServiceLogs
    {
        [Key]
        public int Id { get; set; }
        public DateTime RunDate { get; set; }
        public bool Status { get; set; }
    }
}
