﻿using DangoteSyncService.Models;
using DangoteSyncService.Services;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService
{
    class Agent
    {
        Serilog.Core.Logger logger = Helper.LogConfig();
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public DangoteAuthService.si_full_auth_absResponse Authentication()
        {
            var result = new DangoteAuthService.si_full_auth_absResponse();
            try
            {
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];

                DangoteAuthService.dt_full_auth_in request = new DangoteAuthService.dt_full_auth_in()
                {
                    full_synch = "1",
                    password = password,
                    username = username
                };
                logger.Information("Dangote Authentication:: request - " + Helper.ObjectToXml(request));
                DangoteAuthService.si_full_auth_absClient service = new DangoteAuthService.si_full_auth_absClient();

                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                using (OperationContextScope scope = new OperationContextScope(service.InnerChannel))
                {
                    // Damilola's edit
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(service.ClientCredentials.UserName.UserName + ":" +
                                 service.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    // end of damilola's edit.


                    result = service.si_full_auth_absAsync(request).GetAwaiter().GetResult();
                    logger.Information("Dangote Authentication:: response - " + Helper.ObjectToXml(result));
                }
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Authentication:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return result;
        }

        public async Task SyncAsync()
        {
            try
            {
                bool run = RuntimeLogService.HasRun();
                if (!run)
                {
                    var response = Authentication();
                    if (response.GetType() == typeof(DangoteAuthService.si_full_auth_absResponse))
                    {
                        var result = response.mt_full_auth_out;
                        if (result.error_log.Length < 1)
                        {
                            var records = result.records.ToList();
                            await SaveRecordsAsync(records);
                            var areas = result.credit_areas.ToList();
                            await SaveAreasAsync(areas);
                            var regions = result.regions.ToList();
                            await SaveRegionsAsync(regions);
                            RuntimeLogService.AddLog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Dangote GetToken:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public async Task SaveRecordsAsync(List<DangoteAuthService.dt_full_auth_outRecord> records)
        {
            RecordService service = new RecordService();
            var data = records.ToList().Select(x => new Records
            {
                Base_unit = x.Base_unit,
                company_code = x.company_code,
                division = x.division,
                material = x.material,
                material_description = x.material_description,
                plant = x.plant,
                plant_description = x.plant_description,
                sales_organisation = x.sales_organisation,
                sales_organisation_description = x.sales_organisation,
                sales_unit = x.sales_unit
            }).ToList();
            await service.BulkInsertAsync(data);
        }

        public async Task SaveAreasAsync(IEnumerable<DangoteAuthService.dt_full_auth_outArea> areas)
        {
            AreaService service = new AreaService();
            var data = areas.ToList().Select(x => new Areas
            {
                cbn_code = x.cbn_code,
                company_code = x.company_code,
                credit_area = x.credit_area
            }).ToList();
            await service.BulkInsertAsync(data);
        }

        public async Task SaveRegionsAsync(List<DangoteAuthService.dt_full_auth_outRegion> regions)
        {
            RegionService service = new RegionService();
            var data = regions.ToList().Select(x => new Regions
            {
                country = x.country,
                country_code = x.country_code,
                region_name = x.region_name
            }).ToList();
            await service.BulkInsertAsync(data);
        }
    }
}
