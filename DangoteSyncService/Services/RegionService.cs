﻿using DangoteSyncService.Models;
using DangoteSyncService.Repositories;
using Dapper;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRow = DangoteSyncService.Models.Regions;

namespace DangoteSyncService.Services
{
    class RegionService
    {
        RegionRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public RegionService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new RegionRepository(conn, generator);
        }

        public async Task<int> BulkInsertAsync(IEnumerable<MyRow> regions)
        {
            using (IDbConnection conn = new SqlConnection(DefaultConnection))
            {
                conn.Execute("truncate table tbl_regions");
                return await repo.BulkInsertAsync(regions);
            }
        }
    }
}
