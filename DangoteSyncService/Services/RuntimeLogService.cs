﻿using DangoteSyncService.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService.Services
{
   public static class RuntimeLogService
    {
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static void AddLog()
        {
            using(IDbConnection conn= new SqlConnection(DefaultConnection))
            {
                ServiceLogs logs = new ServiceLogs() { RunDate = DateTime.Now, Status = true };
                conn.Execute("insert into tbl_logs (RunDate, Status) values(@RunDate, @Status)", logs);
            }
        }

        public static bool HasRun()
        {
            using(IDbConnection conn = new SqlConnection(DefaultConnection))
            {
                var result = conn.Query<ServiceLogs>("select * from tbl_logs where convert(varchar, RUNDATE, 103) = CONVERT(varchar, getdate(), 103)").AsList();
                return result.Count > 0;
            }
        }
    }
}
