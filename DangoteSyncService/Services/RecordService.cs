﻿using DangoteSyncService.Models;
using DangoteSyncService.Repositories;
using Dapper;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRow = DangoteSyncService.Models.Records;


namespace DangoteSyncService.Services
{
    class RecordService
    {
        RecordRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public RecordService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new RecordRepository(conn, generator);
        }

        public async Task<int> BulkInsertAsync(IEnumerable<MyRow> records)
        {
            using (IDbConnection conn = new SqlConnection(DefaultConnection))
            {
                conn.Execute("truncate table tbl_records");
                return await repo.BulkInsertAsync(records);
            }
        }
    }
}
