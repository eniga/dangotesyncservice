﻿using DangoteSyncService.Models;
using DangoteSyncService.Repositories;
using Dapper;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRow = DangoteSyncService.Models.Areas;

namespace DangoteSyncService.Services
{
    class AreaService
    {
        AreaRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public AreaService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new AreaRepository(conn, generator);
        }

        public async Task<int> BulkInsertAsync(IEnumerable<MyRow> areas)
        {
            using (IDbConnection conn = new SqlConnection(DefaultConnection))
            {
                conn.Execute("truncate table tbl_areas");
                return await repo.BulkInsertAsync(areas);
            }
        }
    }
}
