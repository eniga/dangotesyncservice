﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DangoteSyncService
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer thisTimer;
        public bool IsRunning;
        public string serviceDesc = "DangoteSyncService";
        Serilog.Core.Logger logger = Helper.LogConfig();

        public Service1()
        {
            InitializeComponent();
        }

        public void dummyStart()
        {
            thisTimer = new System.Timers.Timer();
            thisTimer.Enabled = true;
            int timerInterval = 10800;

            thisTimer.Interval = timerInterval;
            thisTimer.AutoReset = true;
            thisTimer.Elapsed += thisTimer_Tick;
            thisTimer.Start();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            thisTimer = new System.Timers.Timer();
            thisTimer.Enabled = true;
            int timerInterval = 10800;

            logger.Information("DangoteSyncService:: Agent started");
            thisTimer.Interval = timerInterval;
            thisTimer.AutoReset = true;
            thisTimer.Elapsed += thisTimer_Tick;
            thisTimer.Start();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            logger.Information("DangoteSyncService:: Agent Stopped.");
        }

        private void thisTimer_Tick(System.Object sender, System.EventArgs e)
        {
            DoNextExecution();
        }

        private void DoNextExecution()
        {
            lock (this)
            {
                thisTimer.Stop();
                try
                {
                    processNextTransactionAsync().GetAwaiter();
                }
                catch (Exception ex)
                {
                    logger.Error("DangoteSyncService:: " + ex.Message);
                }

                int timerInterval = 10800;
                thisTimer.Interval = timerInterval;
                thisTimer.Start();
            }
        }

        private async Task processNextTransactionAsync()
        {
            try
            {
                Agent agent = new Agent();
                await agent.SyncAsync();
            }
            catch (Exception ex)
            {
                logger.Error("DangoteSyncService:: " + ex.Message);
            }
        }
    }
}
